package com.abs.ethio_recipes

import android.os.Bundle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.String
import kotlin.jvm.JvmStatic

data class RecipeDetailFragmentArgs(val title: String) : NavArgs {
    fun toBundle(): Bundle {
        val result = Bundle()
        result.putString("title", this.title)
        return result
    }

    companion object {
        @JvmStatic
        fun fromBundle(bundle: Bundle): RecipeDetailFragmentArgs {
            bundle.setClassLoader(RecipeDetailFragmentArgs::class.java.classLoader)
            val __title : String?
            if (bundle.containsKey("title")) {
                __title = bundle.getString("title")
                if (__title == null) {
                    throw IllegalArgumentException("Argument \"title\" is marked as non-null but was passed a null value.")
                }
            } else {
                throw IllegalArgumentException("Required argument \"title\" is missing and does not have an android:defaultValue")
            }
            return RecipeDetailFragmentArgs(__title)
        }
    }
}
