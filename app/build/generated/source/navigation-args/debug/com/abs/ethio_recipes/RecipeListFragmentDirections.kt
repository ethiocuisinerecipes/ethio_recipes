package com.abs.ethio_recipes

import android.os.Bundle
import androidx.navigation.NavDirections
import kotlin.Int
import kotlin.String

class RecipeListFragmentDirections private constructor() {
    private data class ActionRecipeListFragmentToRecipeDetailFragment(val title: String) :
            NavDirections {
        override fun getActionId(): Int = R.id.action_recipeListFragment_to_recipeDetailFragment

        override fun getArguments(): Bundle {
            val result = Bundle()
            result.putString("title", this.title)
            return result
        }
    }

    companion object {
        fun actionRecipeListFragmentToRecipeDetailFragment(title: String): NavDirections =
                ActionRecipeListFragmentToRecipeDetailFragment(title)
    }
}
