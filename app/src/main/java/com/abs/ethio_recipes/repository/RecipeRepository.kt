package com.abs.ethio_recipes.repository

import com.abs.ethio_recipes.data.Recipe
import com.abs.ethio_recipes.data.RecipeDao

class RecipeRepository(private val recipeDao: RecipeDao) {

    fun getRecipes() = recipeDao.getAllRecipes()

    fun getRecipe(title: String) = recipeDao.getRecipe(title)

    fun insertRecipe(recipe: Recipe) {
        recipeDao.insertRecipe(recipe)

    }

    fun deleteRecipe(recipe: Recipe) {
        recipeDao.deleteRecipe(recipe)

    }
    fun updateRecipe(recipe: Recipe) {
        recipeDao.updateRecipe(recipe)

    }

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: RecipeRepository? = null

        fun getInstance(recipeDao: RecipeDao) =
            instance ?: synchronized(this) {
                instance ?: RecipeRepository(recipeDao).also { instance = it }
            }
    }
}

