package com.abs.ethio_recipes.repository


import com.abs.ethio_recipes.data.ApiService
import com.abs.ethio_recipes.data.Recipe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class RecipePostRepository(private val apiService: ApiService) {

    suspend fun getRecipeByTitle(title: Long): Response<Recipe> =
        withContext(Dispatchers.IO){
            apiService.getRecipeByTitle(title).await()
        }
    suspend fun updateRecipe(title: Long, recipe: Recipe): Response<Recipe> =
        withContext(Dispatchers.IO){
            apiService.updateRecipe(title, recipe).await()
        }
    suspend fun insertRecipe(recipe: Recipe): Response<Recipe> =
        withContext(Dispatchers.IO) {
            apiService.insertRecipe(recipe).await()
        }
    suspend fun deleteRecipe(title: Long): Response<Void> =
        withContext(Dispatchers.IO){
            apiService.deleteRecipe(title).await()
        }
}

