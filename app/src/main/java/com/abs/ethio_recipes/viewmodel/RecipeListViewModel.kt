package com.abs.ethio_recipes.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.abs.ethio_recipes.data.Recipe
import com.abs.ethio_recipes.repository.RecipeRepository

class RecipeListViewModel internal constructor(recipeRepository: RecipeRepository) : ViewModel (){

    val recipes : LiveData<List<Recipe>> = recipeRepository.getRecipes()


}