package com.abs.ethio_recipes.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.abs.ethio_recipes.data.ApiService
import com.abs.ethio_recipes.data.Recipe
import com.abs.ethio_recipes.repository.RecipePostRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class PostViewModel: ViewModel(){

    private val postRepository: RecipePostRepository

    init{
        val apiService = ApiService.getInstance()
        postRepository = RecipePostRepository(apiService)
    }

    private  val _getResponse = MutableLiveData<Response<Recipe>>()
    val getResponse: LiveData<Response<Recipe>>
        get() = _getResponse

    private val _getResponses = MutableLiveData<Response<List<Recipe>>>()
    val getResponses: LiveData<Response<List<Recipe>>>
        get() = _getResponses
    private val _updateResponse = MutableLiveData<Response<Recipe>>()
    val updateResponse: LiveData<Response<Recipe>>
        get() = _updateResponse
    private val _insertResponse = MutableLiveData<Response<Recipe>>()
    val insertResponse: LiveData<Response<Recipe>>
        get() = _insertResponse
    private val _deleteResponse = MutableLiveData<Response<Void>>()
    val deleteResponse: MutableLiveData<Response<Void>>
        get() = _deleteResponse

    fun getRecipeByTitle(title: Long) = viewModelScope.launch{
        _getResponse.postValue(postRepository.getRecipeByTitle(title))
    }

    fun updateRecipe(title: Long,recipe: Recipe) = viewModelScope.launch {
        _updateResponse.postValue(postRepository.updateRecipe(title, recipe))
    }

    fun insertRecipe(recipe: Recipe) = viewModelScope.launch {
        _insertResponse.postValue(postRepository.insertRecipe(recipe))
    }

    fun deleteRecipe(title: Long) = viewModelScope.launch {
        _deleteResponse.postValue(postRepository.deleteRecipe(title))
    }

}