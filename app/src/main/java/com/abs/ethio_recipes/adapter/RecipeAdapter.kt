package com.abs.ethio_recipes.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.abs.ethio_recipes.data.Recipe
import com.abs.ethio_recipes.RecipeListFragmentDirections
import com.abs.ethio_recipes.databinding.RecipeItemListBinding

/**
 * adapter for the [RecyclerView] in [PlantListFragment].
 */
class RecipeAdapter : ListAdapter<Recipe, RecipeAdapter.ViewHolder>(RecipeDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val recipe = getItem(position)
        holder.apply {
            bind(createOnClickListener(recipe.title), recipe)
            itemView.tag = recipe
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RecipeItemListBinding.inflate(
            LayoutInflater.from(parent.context), parent, false))
    }

    private fun createOnClickListener(title: String): View.OnClickListener {
        return View.OnClickListener {
            val direction = RecipeListFragmentDirections.actionRecipeListFragmentToRecipeDetailFragment(title)
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
        private val binding: RecipeItemListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, item: Recipe) {
            binding.apply {
                clickListener = listener
                recipe = item
                executePendingBindings()
            }
        }
    }
}

private class RecipeDiffCallback : DiffUtil.ItemCallback<Recipe>() {

    override fun areItemsTheSame(oldItem: Recipe, newItem: Recipe): Boolean {
        return oldItem.title == newItem.title
    }

    override fun areContentsTheSame(oldItem: Recipe, newItem: Recipe): Boolean {
        return oldItem == newItem
    }
}