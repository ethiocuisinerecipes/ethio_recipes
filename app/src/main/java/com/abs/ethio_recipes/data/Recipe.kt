package com.abs.ethio_recipes.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "recipes")
data class Recipe(@PrimaryKey @ColumnInfo(name = "Recipe_Title")val title:String,
                  @ColumnInfo(name = "Summery")val summery:String,
                  @ColumnInfo(name = "Ingredients")val ingredients:String,
                  @ColumnInfo(name = "Preparation")val preparation:String)
    :Serializable