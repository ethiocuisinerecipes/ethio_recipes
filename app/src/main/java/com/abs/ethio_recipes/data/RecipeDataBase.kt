package com.abs.ethio_recipes.data

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase

abstract class RecipeDatabase : RoomDatabase() {
    abstract fun recipeDao():RecipeDao

    companion object {

        @Volatile
        private var INSTANCE: RecipeDatabase? = null

        fun getDatabase(context: Context): RecipeDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {

                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    RecipeDatabase::class.java, "recipe_database"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }
}