package com.abs.ethio_recipes.data


import androidx.room.Query
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface ApiService {

    @GET("posts/{title}")
    fun getRecipeByTitle(@Path("title") title: Long): Deferred<Response<Recipe>>

    @POST("posts")
    fun insertRecipe(@Body recipe: Recipe): Deferred<Response<Recipe>>

    @PUT("posts/{title}")
    fun updateRecipe(@Path("title") title: Long, @Body recipe: Recipe): Deferred<Response<Recipe>>

    @DELETE("posts/{title}")
    fun deleteRecipe(@Path("title") title: Long): Deferred<Response<Void>>

    companion object {

        private val baseUrl = "https://jsonplaceholder.typicode.com/"

        fun getInstance(): ApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(ApiService::class.java)

        }

    }

}

