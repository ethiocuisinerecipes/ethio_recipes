package com.abs.ethio_recipes.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface RecipeDao {

    @Query("SELECT * from recipes ORDER BY Recipe_Title")
    fun getAllRecipes(): LiveData<List<Recipe>>

    @Query("SELECT * FROM recipes WHERE Recipe_Title = :title")
    fun getRecipe(title: String): LiveData<Recipe>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecipe(recipe: Recipe):Long

    @Update
    fun updateRecipe(recipe: Recipe):Long

    @Delete
    fun deleteRecipe(recipe: Recipe):Long
}