package com.abs.ethio_recipes


import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.ViewModelProviders
import com.abs.ethio_recipes.data.Recipe
import com.abs.ethio_recipes.viewmodel.PostViewModel
import kotlinx.android.synthetic.main.fragment_add_recipe.*
import java.util.*


class AddRecipe : Fragment() {



    private fun readFields() = Recipe(
        title_edit_text.text.toString(),
        summery_edit_text.text.toString(),
        ingredients_edit_text.text.toString(),
        preparation_edit_text.text.toString()
    )

    private fun updateFields(recipe: Recipe){
        recipe.run{
            title_edit_text.setText(title.toString())
            summery_edit_text.setText(summery.toString())
            ingredients_edit_text.setText(ingredients.toString())
            preparation_edit_text.setText(preparation.toString())
        }
    }

    private fun clearFields(){
        title_edit_text.setText("")
        summery_edit_text.setText("")
        ingredients_edit_text.setText("")
        preparation_edit_text.setText("")
    }

    private fun connected():Boolean{
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val postViewModel = ViewModelProviders.of(this).get(PostViewModel::class.java)

        add_button.setOnClickListener{
            val recipe = readFields()
            if(connected()){
                postViewModel.insertRecipe(recipe)
                postViewModel.insertResponse.observe(this, androidx.lifecycle.Observer { response ->
                    response.body()?.run{
                        updateFields(this)
                    }
                })
            }
            clearFields()
        }

        update_button.setOnClickListener{
            val recipe = readFields()
            if(connected()){
                postViewModel.updateRecipe(recipe.title, recipe)
                postViewModel.insertResponse.observe(this, androidx.lifecycle.Observer { response ->
                    response.body()?.run{
                        updateFields(this)
                    }
                })
            }
            clearFields()
        }


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_recipe, container, false)
    }


}
